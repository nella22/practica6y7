package pico.jennfier.facci.practica6y7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button sensores, vibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensores = findViewById(R.id.btnVibrar);

        sensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(
                        MainActivity.this, ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        vibrar = findViewById(R.id.btnVibrar);

        vibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActividadVibrar.class);
                startActivity(intent);
            }
        });
    }
}
